'use strict'; // --!-- function def =============================

function animateBlock(el) {
  var delay = 0;
  var anim = $(el.element).find('.anim');
  anim.each(function () {
    $(this).addClass('animate-block');
    $(this).css('animation-delay', delay + 'ms');
    delay += 200;
  });
} // ymaps init


function init() {
  var myMap = new ymaps.Map("map", {
    center: [56.301813, 44.077748],
    zoom: 14,
    controls: ['zoomControl']
  }),
      myGeoObject = new ymaps.GeoObject({
    geometry: {
      type: "Point",
      // тип геометрии - точка
      coordinates: [56.301813, 44.077748] // координаты точки

    }
  });
  myMap.geoObjects.add(myGeoObject);
  myMap.behaviors.disable('scrollZoom');

  if ($(window).width() <= 768) {
    myMap.behaviors.disable('drag');
  }
} // --!-- init plugins =============================


$(document).ready(function () {
  // loader
  $(window).on('load', function () {
    $('#preloader').fadeOut(function () {
      $(this).remove();
    });
    $('body').removeClass('no-scroll');
  });
  setTimeout(function () {
    if ($("#preloader").length) {
      $("#preloader").fadeOut(function () {
        $(this).remove();
      });
      $('body').removeClass('no-scroll');
    }
  }, 1000); // imgLiquid init

  $(".imgLiquidFill").imgLiquid({
    fill: true,
    horizontalAlign: "center",
    verticalAlign: "center"
  });
  $(".imgLiquidNoFill").imgLiquid({
    fill: false,
    horizontalAlign: "center",
    verticalAlign: "center"
  }); // input tel mask init

  $("#inputPhone").mask("?+7 (999) 999-9999"); // header menu btn toggle

  $('.navbar-menu-btn').on('click', function (e) {
    e.stopPropagation();
    $('.navbar-menu-btn, .navbar-menu, .navbar-menu-overlay').toggleClass('active');
    $('body, html').toggleClass('no-scroll');
  });
  $('.navbar-menu-overlay').on('click', function (e) {
    $('.navbar-menu-btn, .navbar-menu, .navbar-menu-overlay').removeClass('active');
    $('body, html').removeClass('no-scroll');
  }); // search site btn toggle

  $('.search-btn').on('click', function () {
    $('.search-input, .search-overlay').toggleClass('shown');
    $('body, html').toggleClass('no-scroll');
  });
  $('.search-overlay').on('click', function () {
    $('.search-input, .search-overlay').removeClass('shown');
    $('body, html').removeClass('no-scroll');
  }); // pages logic ==================================

  if ($(".index-page").length) {
    var swiper_1 = new Swiper('.slider-block .swiper-container', {
      loop: true,
      slidesPerView: 'auto',
      centeredSlides: true,
      spaceBetween: 30,
      speed: 1000,
      autoplay: {
        delay: 4000,
        disableOnInteraction: false
      }
    });
    var swiper_2 = new Swiper('.insta-block .swiper-container', {
      loop: true,
      slidesPerView: 'auto',
      centeredSlides: true,
      spaceBetween: 30,
      speed: 750,
      autoplay: {
        delay: 4000,
        disableOnInteraction: false
      }
    });
    $('main section').each(function () {
      $(this).waypoint(function (direction) {
        animateBlock(this);
      }, {
        offset: '50%'
      });
    });
  }

  if ($(".news_detail-page").length) {
    if ($(".slider-block").length) {
      $('.slider-block').each(function (index, element) {
        var $this = $(this);
        $this.addClass("slider-block-swiper-" + index);
        var galleryTop = new Swiper('.slider-block-swiper-' + index + ' .gallery-top', {
          spaceBetween: 10,
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
          }
        });
        var galleryThumbs = new Swiper('.slider-block-swiper-' + index + ' .gallery-thumbs', {
          spaceBetween: 10,
          slidesPerView: 'auto',
          centeredSlides: true,
          touchRatio: 0.2,
          slideToClickedSlide: true
        });
        galleryTop.controller.control = galleryThumbs;
        galleryThumbs.controller.control = galleryTop;
      });
    }

    if ($(".project-block").length) {
      $('.project-block').each(function (index, element) {
        var $this = $(this);
        $this.addClass("project-block-swiper-" + index);
        var projectSwiper = new Swiper('.project-block-swiper-' + index + ' .swiper-container', {
          spaceBetween: 10,
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
          },
          pagination: {
            el: ".swiper-pagination",
            dynamicBullets: false
          }
        });
      });
    }

    if ($("#twenty").length) {
      var isotopeWrap = $('#isotope').isotope({
        itemSelector: '.element-item',
        layoutMode: 'fitRows',
        fitRows: {
          columnWidth: '.element-item',
          gutter: ".gutter-sizer"
        },
        //masonry: {
        //    columnWidth: '.element-item',
        //    gutter: '.gutter-sizer'
        //},
        getSortData: {
          category: '[data-category]'
        }
      });
      setTimeout(function () {
        isotopeWrap.isotope('reloadItems');
      }, 500);
      $("#twenty").twentytwenty({
        before_label: 'До',
        // Set a custom before label
        after_label: 'После'
      });
      var filterFns = {};
      $('#filters').on('click', '.filter-button', function () {
        var filterValue = $(this).attr('data-filter');
        filterValue = filterFns[filterValue] || filterValue;
        isotopeWrap.isotope({
          filter: filterValue
        });
      });
      $('.element-item').on('click', function () {
        var before = $(this).find('.hidden-img').data('beforeimg');
        var after = $(this).find('.hidden-img').data('afterimg');
        var sub = $(this).find(".hidden-img").data("sub");
        var text = $(this).find(".hidden-img").data("text");
        $(".twenty-wrap").empty();
        $(".twenty-text").empty();
        $(".twenty-wrap").addClass('hidden-load');
        $(".twenty-wrap").append('<div id="twenty" class="unvis"><img src="' + before + '"><img src="' + after + '"></div>');
        $(".twenty-text").append('<div id="twenty-sub" class="sub unvis">' + sub + '</div><div id="twenty-text" class="text unvis">' + text + '</div>');
        $(".twenty-wrap img").on('load', function () {
          setTimeout(function () {
            $("#twenty").twentytwenty({
              before_label: 'До',
              after_label: 'После'
            });
            $(".twenty-wrap").removeClass('hidden-load');
            $('html, body').animate({
              scrollTop: $(".twenty-wrap").offset().top - 80
            }, 500);
          }, 200);
        });
      });
    }
  }

  if ($(".contacts-page").length) {
    var map = ymaps.ready(init);
  }

  if ($(".club-page").length) {
    $('.input-file .btn').on('click', function () {
      $('.input-file input').click();
    });
    $('.input-file input').on('change', function () {
      var filename = $(this).val().split('\\').pop();
      if (filename) $(".input-file .upload-file-info").html(filename);else $(".input-file .upload-file-info").html('DOC, ZIP, PDF<br/>до 50 Мб');
    });
  }

  if ($(".gallery-page").length) {
    $('[data-fancybox="images"]').fancybox({
      baseClass: '.gallery-fancybox',
      idleTime: 0,
      infobar: false,
      buttons: ['close'],
      parentEl: '.gallery-page .gallery-block',
      animationEffect: "fade",
      animationDuration: 300,
      caption: function caption(instance, obj) {
        return $(this).find('.caption').html();
      },
      btnTpl: {
        close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' + '<svg viewBox="0 0 44 44">' + '<polygon style="fill:#FFFFFF;" points="44,1.419 42.581,0 22,20.581 1.419,0 0,1.419 20.581,22 0,42.581 1.419,44 22,23.419 42.581,44 44,42.581 23.419,22 "/>' + '</svg>' + '</button>',
        arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' + '<svg viewBox="0 0 21 44">' + '<polygon style="fill:#FFFFFF;" points="20,0 21,1 2,21.992 21,43 20,44 0,21.964 "/>' + '</svg>' + '</button>',
        arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' + '<svg viewBox="0 0 21 44">' + '<polygon style="fill:#FFFFFF;" points="1,0 0,1 19,21.992 0,43 1,44 21,21.964 "/>' + '</svg>' + '</button>'
      },
      baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1">' + '<div class="fancybox-bg"></div>' + '<div class="fancybox-inner">' + '<div class="fancybox-infobar">' + '<span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span>' + '</div>' + '<div class="fancybox-toolbar">{{buttons}}</div>' + '<div class="fancybox-navigation">{{arrows}}</div>' + '<div class="fancybox-outer">' + '<div class="fancybox-stage"></div>' + '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' + '</div>' + '</div>' + '</div>',
      onInit: function onInit(instance, slide) {//console.log($(this))
        //var caption = $(instance.current.opts.$orig).find('.caption');
        //console.log(caption)
        //$(caption).appendTo(slide.$content)
        //instance.$refs.inner.wrap( '<div class="fancybox-outer"></div>' );
      },
      lang: 'ru',
      i18n: {
        'en': {
          CLOSE: 'Close',
          NEXT: 'Next',
          PREV: 'Previous',
          ERROR: 'The requested content cannot be loaded. <br/> Please try again later.',
          PLAY_START: 'Start slideshow',
          PLAY_STOP: 'Pause slideshow',
          FULL_SCREEN: 'Full screen',
          THUMBS: 'Thumbnails',
          DOWNLOAD: 'Download',
          SHARE: 'Share',
          ZOOM: 'Zoom'
        },
        'ru': {
          CLOSE: 'Закрыть',
          NEXT: 'Следующая',
          PREV: 'Предыдущая',
          ERROR: 'Ошибка при загрузке! <br/> Попробуйте перезагрузить страницу.',
          PLAY_START: 'Начать просмотр',
          PLAY_STOP: 'Остановить просмотр',
          FULL_SCREEN: 'Во всет экран',
          THUMBS: 'Предпросмотр',
          DOWNLOAD: 'Загрузить',
          SHARE: 'Поделиться',
          ZOOM: 'Увеличить'
        }
      }
    });
  }

  if ($(".gallery_detail-page").length) {
    $('[data-fancybox="image"]').fancybox({
      baseClass: '.detail-fancybox',
      idleTime: 0,
      infobar: false,
      parentEl: '.gallery_detail-page .detail-block',
      buttons: ['close'],
      animationEffect: "fade",
      animationDuration: 300,
      btnTpl: {
        close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' + '<svg viewBox="0 0 44 44">' + '<polygon style="fill:#FFFFFF;" points="44,1.419 42.581,0 22,20.581 1.419,0 0,1.419 20.581,22 0,42.581 1.419,44 22,23.419 42.581,44 44,42.581 23.419,22 "/>' + '</svg>' + '</button>',
        arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' + '<svg viewBox="0 0 21 44">' + '<polygon style="fill:#FFFFFF;" points="20,0 21,1 2,21.992 21,43 20,44 0,21.964 "/>' + '</svg>' + '</button>',
        arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' + '<svg viewBox="0 0 21 44">' + '<polygon style="fill:#FFFFFF;" points="1,0 0,1 19,21.992 0,43 1,44 21,21.964 "/>' + '</svg>' + '</button>'
      },
      lang: 'ru',
      i18n: {
        'en': {
          CLOSE: 'Close',
          NEXT: 'Next',
          PREV: 'Previous',
          ERROR: 'The requested content cannot be loaded. <br/> Please try again later.',
          PLAY_START: 'Start slideshow',
          PLAY_STOP: 'Pause slideshow',
          FULL_SCREEN: 'Full screen',
          THUMBS: 'Thumbnails',
          DOWNLOAD: 'Download',
          SHARE: 'Share',
          ZOOM: 'Zoom'
        },
        'ru': {
          CLOSE: 'Закрыть',
          NEXT: 'Следующая',
          PREV: 'Предыдущая',
          ERROR: 'Ошибка при загрузке! <br/> Попробуйте перезагрузить страницу.',
          PLAY_START: 'Начать просмотр',
          PLAY_STOP: 'Остановить просмотр',
          FULL_SCREEN: 'Во всет экран',
          THUMBS: 'Предпросмотр',
          DOWNLOAD: 'Загрузить',
          SHARE: 'Поделиться',
          ZOOM: 'Увеличить'
        }
      }
    });
    $('[data-fancybox="images"]').fancybox({
      baseClass: '.popular-fancybox',
      idleTime: 0,
      infobar: false,
      buttons: ['close'],
      parentEl: '.gallery_detail-page .popular-gallery',
      animationEffect: "fade",
      animationDuration: 300,
      caption: function caption(instance, obj) {
        return $(this).find('.caption').html();
      },
      btnTpl: {
        close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}">' + '<svg viewBox="0 0 44 44">' + '<polygon style="fill:#FFFFFF;" points="44,1.419 42.581,0 22,20.581 1.419,0 0,1.419 20.581,22 0,42.581 1.419,44 22,23.419 42.581,44 44,42.581 23.419,22 "/>' + '</svg>' + '</button>',
        arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}">' + '<svg viewBox="0 0 21 44">' + '<polygon style="fill:#FFFFFF;" points="20,0 21,1 2,21.992 21,43 20,44 0,21.964 "/>' + '</svg>' + '</button>',
        arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}">' + '<svg viewBox="0 0 21 44">' + '<polygon style="fill:#FFFFFF;" points="1,0 0,1 19,21.992 0,43 1,44 21,21.964 "/>' + '</svg>' + '</button>'
      },
      baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1">' + '<div class="fancybox-bg"></div>' + '<div class="fancybox-inner">' + '<div class="fancybox-infobar">' + '<span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span>' + '</div>' + '<div class="fancybox-toolbar">{{buttons}}</div>' + '<div class="fancybox-navigation">{{arrows}}</div>' + '<div class="fancybox-outer">' + '<div class="fancybox-stage"></div>' + '<div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div>' + '</div>' + '</div>' + '</div>',
      onInit: function onInit(instance, slide) {
        console.log($(this)); //var caption = $(instance.current.opts.$orig).find('.caption');
        //console.log(caption)
        //$(caption).appendTo(slide.$content)
        //instance.$refs.inner.wrap( '<div class="fancybox-outer"></div>' );
      },
      lang: 'ru',
      i18n: {
        'en': {
          CLOSE: 'Close',
          NEXT: 'Next',
          PREV: 'Previous',
          ERROR: 'The requested content cannot be loaded. <br/> Please try again later.',
          PLAY_START: 'Start slideshow',
          PLAY_STOP: 'Pause slideshow',
          FULL_SCREEN: 'Full screen',
          THUMBS: 'Thumbnails',
          DOWNLOAD: 'Download',
          SHARE: 'Share',
          ZOOM: 'Zoom'
        },
        'ru': {
          CLOSE: 'Закрыть',
          NEXT: 'Следующая',
          PREV: 'Предыдущая',
          ERROR: 'Ошибка при загрузке! <br/> Попробуйте перезагрузить страницу.',
          PLAY_START: 'Начать просмотр',
          PLAY_STOP: 'Остановить просмотр',
          FULL_SCREEN: 'Во всет экран',
          THUMBS: 'Предпросмотр',
          DOWNLOAD: 'Загрузить',
          SHARE: 'Поделиться',
          ZOOM: 'Увеличить'
        }
      }
    });
  }
});